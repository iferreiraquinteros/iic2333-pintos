#include "userprog/syscall.h"
#include <stdio.h>
#include <syscall-nr.h>
#include "threads/interrupt.h"
#include "threads/thread.h"

static void syscall_handler (struct intr_frame *);

void
syscall_init (void) 
{
  intr_register_int (0x30, 3, INTR_ON, syscall_handler, "syscall");
}

static void
syscall_handler (struct intr_frame *f) 
{
  printf ("SYSCALL HANDLER: System call!!\n");
  printf("        ESP at: %p\n", f->esp);
  printf("Content at ESP: *(%p): %d\n", f->esp, *((int *)f->esp));
  printf("Content  ESP+4: *(%p): %d\n", f->esp+4, *((int *)(f->esp+4)));
  printf("Content  ESP+8: *(%p): %d\n", f->esp+8, *((int *)(f->esp+8)));
  printf("Content ESP+12: *(%p): %d\n", f->esp+12, *((int *)(f->esp+12)));
  
  int syscallNumber = *((int*)(f->esp));
  printf ("syscall: %d\n", syscallNumber);
  

  if(syscallNumber == 0) {
    printf("Implemento HALT\n");
    int *p = f->esp;
    hex_dump((int)(p), p, 16, true);

  }
  else if(syscallNumber == 1) {
    printf("Implemento EXIT\n");
    int *p = f->esp;
    hex_dump((int)(p), p, 16, true);

  }
  else if(syscallNumber == 9) {
    printf("Implemento WRITE\n");
    /*
    while(next){
      aux= (char)p;
      p++;
      if(aux != '\0'){
        printf(aux);
      }
      else{
        aux = (char)p;
        p++;
        if(aux == '\0'){
          next = false;
        }
        else{
          printf(aux);
        }
      }
    }
    printf("\n");
    */
  }
  thread_exit ();
}
